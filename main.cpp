#include <iostream>
#include "Document.h"
//12. Квитанция, накладная, документ, счет.

int main() {
    auto receipt = new Receipt();
    receipt->setReceiver("Ivanov");
    receipt->setService("some service");
    auto bill = new Bill();
    bill->addProduct("asdasd", 111, 2);
    auto invoice = new Invoice();
    invoice->addProduct("asdsasdasdasd", 1233, 1);

    Document::print();
    return 0;
}