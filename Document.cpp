//
// Created by Alexander Solovyov on 2020-02-29.
//

#include "Document.h"
#include <utility>
#include <vector>
#include <tuple>

int Document::VAT = 20;
Node<Document> *Document::first = nullptr;

Document::Document() {
    this->add();
}

Document::Document(string name) : name(std::move(name)) {}

Document::Document(string name, time_t date) : name(std::move(name)), date(date) {}

Document::Document(string name, time_t date, double total) : name(std::move(name)), date(date), total(total) {}

void Document::add() {
    Node<Document> *node = new Node<Document>;
    node->value = this;
    if (first == nullptr) {
        first = node;
    } else {
        auto temp = first;
        while (temp->next != nullptr) temp = temp->next;
        temp->next = node;

    }
}

void Document::show() {
    cout << "Document: " << name << endl;
}

void Receipt::show() {
    cout << "Receipt for service: " << serviceName << ", Receiver: " << receiver << endl;
}


void Bill::addProduct(string name, double price, int count) {
    if (count <= 0) count = 1;
    if (price < 0) price = 0;
    setTotal(total + price * count);
    products.emplace_back(name, price, count);
}

void Bill::removeProduct(string name) {
    for (int i = 0; i < products.size(); ++i) {
        auto p = products[i];
        if (get<0>(p) == name) {
            setTotal(total - get<1>(p) * get<2>(p));
            products.erase(products.begin() + i);
        }
    }
}

void Bill::show() {
    cout << "Bill" << " count: " << products.size() << ", total: " << getTotal() << endl;
}

void Invoice::show() {
    cout << "Invoice" << " count: " << products.size() << ", total: " << getTotal()
         << ", total with VAT: " << getTotalWithVAT() << endl;
}
