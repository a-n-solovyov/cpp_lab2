//
// Created by Alexander Solovyov on 2020-02-29.
//
#include <string>
#include <iostream>
#include <list>
#include <utility>
#include <vector>
#include <tuple>


using namespace ::std;

#ifndef LAB2_2_DOCUMENT_H
#define LAB2_2_DOCUMENT_H

template<typename T>
struct Node {
    T *value = nullptr;
    Node<T> *next = nullptr;
};

class Document {
    static Node<Document> *first;
protected:
    string name;
    time_t date{};
    double total{};
public:
    // percent
    static int VAT;

    Document();

    Document(string name);

    Document(string name, time_t date);

    Document(string name, time_t date, double total);

    virtual void setTotal(double val) {
        if (val >= 0)
            total = val;
    }

    virtual double getTotal() const {
        return total;
    }

    virtual double getTotalWithVAT() const {
        return total * (1 + VAT / 100.0);
    }

    void add();

    virtual void show();

    static void print() {
        Node<Document> *node = Document::first;
        while (node != nullptr) {
            node->value->show();
            node = node->next;
        }
    }
};

// квитанция
class Receipt : Document {
    // услуга
    string serviceName;
    string receiver;

public:
    Receipt() = default;

    void setService(string name) {
        if (!name.empty())
            serviceName = name;
    }

    string getService() {
        return serviceName;
    }

    void setReceiver(string name) {
        if (!name.empty())
            receiver = name;
    }

    string getReceiver() {
        return receiver;
    }

    void show() override;
};

typedef tuple<string, double, int> bill_line;

// счет
class Bill : public Document {
protected:
    vector<bill_line> products;
public:

    Bill() = default;

    vector<bill_line> getProducts() {
        return products;
    }

    void addProduct(string name, double price, int count = 1);

    void removeProduct(string name);


    void show() override;
};


// накладная
class Invoice : public Bill {
    string receiver;
public:
    Invoice() : Bill() {}

    string getReceiver() {
        return receiver;
    }

    void setReceiver(string val) {
        receiver = val;
    }

    void show() override;
};

#endif //LAB2_2_DOCUMENT_H
